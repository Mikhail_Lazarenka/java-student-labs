package labs.lab1.task;

/*
Создать 2-3 объекта класса согласно заданию. Реализовать сеттеры и геттеры.
Вывести на экран монитора соответствующую информацию об объектах.
 Состав класса Music (музыкальное произведение): название произведения,
 количество проданных копий, возможность скачать mp3 в интернете.
 */
public class Lab1 {
    public static void main(String[] args) {
        Music d1 = new Music();  //создание объекта d1
        Music d2 = new Music();  //создание объекта d2

        d1.setName("T-Fest - Злой Мишка");
        d1.setMp3Available(true);
        d1.setCopies(45256523); //заполнение полей объекта d1 с помощью сеттеров
        String s = d1.getName(); //получение значения поля name с помощью геттера
        System.out.println("Информация о треке: " + s);
        d1.Print();//вывод информации об объекте d1

        //с объектом d2 выполняем аналогичные действия
        d2.setName("kickdrum - DEADLINE");
        d2.setMp3Available(false);
        d2.setCopies(34325);
        s = d2.getName();
        System.out.println("Информация о треке: " + s);
        d2.Print();
    }
}
