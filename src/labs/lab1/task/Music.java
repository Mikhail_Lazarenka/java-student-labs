package labs.lab1.task;

public class Music {
    private String name;
    private boolean mp3Available;
    private int copies;

    public Music(String name) {
        this.name = name;
    }

    public Music() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMp3Available(boolean mp3Available) {
        this.mp3Available = mp3Available;
    }

    public void setCopies(int copies) {
        this.copies = copies;
    }

    public String getName() {
        return name;
    }

    public boolean isMp3Available() {
        return mp3Available;
    }

    public int getCopies() {
        return copies;
    }

    public void Print (){
        System.out.println("-- Track name: " + name);
        if (mp3Available) {
            System.out.println("Mp3 available.");
        } else {
            System.out.println("The track isn't mp3 available.");
        }
        System.out.println("Copies sold: " + copies);
    }
}
