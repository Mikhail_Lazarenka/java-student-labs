package labs.lab1.example;
/*
Создать несколько объектов класса согласно заданию. Значение полей задать с помощью сеттеров. В классе предусмотреть геттеры и метод, осуществляющий вывод на экран монитора соответствующую информацию об объекте.
Состав класса Drugstore (аптека): фамилия вла¬дельца, наличие лицензии, месячная прибыль.

 */
public class Lab1Example {
    public static void main(String[] args) {

        Drugstore d1 = new Drugstore();  //создание объекта d1
        Drugstore d2 = new Drugstore();  //создание объекта d2

        d1.setName("Nordin");
        d1.setLicense(true);
        d1.setProfit(1000);//заполнение полей объекта d1 с помощью сеттеров
        String s = d1.getName();//получение значения поля name с помощью геттера
        System.out.println("Информация об аптеке " + s);
        d1.Print();//вывод информации об объекте d1

        //с объектом d2 выполняем аналогичные действия
        d2.setName("Kravira");
        d2.setLicense(false);
        d2.setProfit(2000);
        s = d2.getName();
        System.out.println("Информация об аптеке " + s);
        d2.Print();
    }
}
