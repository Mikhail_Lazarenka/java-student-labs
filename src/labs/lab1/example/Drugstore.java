package labs.lab1.example;

public class Drugstore {
    private String name;
    private boolean license;
    private int profit;

    public void setName(String name) { // метод присваивает подпроцессу указанное в параметре имя
        this.name = name;
    }

    public void setLicense(boolean license) { //аналогично с наличием лицензии {
        this.license = license;
    }

    public void setProfit(int profit) { //аналогично с суммой дохода {
        this.profit = profit;
    }

    public Drugstore() {
    }


    public String getName() {//метод возвращает строку с именем подпроцесса, установленным с помощью вызова setName {
        return name;
    }

    public boolean isLicense() {
        return license;
    }

    public int getProfit() {
        return profit;
    }

    public void Print() {
        System.out.println("Название: " + name);
        if (license) {
            System.out.println("Есть лицензия.");
        } else {
            System.out.println("Нет лецензии.");
        }
        System.out.println("Доход: " + profit);
    }


}

