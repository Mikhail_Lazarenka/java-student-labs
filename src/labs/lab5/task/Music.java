package labs.lab5.task;

import java.io.*;

public class Music {
    private String name;
    private boolean mp3Available;
    private double copies;

    Music() throws IOException {
        String bolv;
        //открываем символьный поток ввода
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in, "Utf-8"));
        System.out.print("\nВведите название трека: ");
        this.name = input.readLine();
        System.out.print("\nВведите количество проданных копий: ");
        this.copies = Double.parseDouble(input.readLine());
        while (true) {
            System.out.print("\nВозможность скачать в формате mp3(Да/Нет): ");
            bolv = input.readLine();
            if ("Да".equals(bolv)) {
                this.mp3Available = true;
                break;
            }
            if ("Нет".equals(bolv)) {
                this.mp3Available = false;
                break;
            }
            System.out.print("\nОшибка! Повторите ввод");
        }
    }

    //записываем информацию в файл document.doc
    public void InputInFile() throws IOException {
        File file = new File("document.doc");
        file.deleteOnExit(); //файл удалится после завершения работы виртуальной машины Java
        //поток для записи в файл
        FileWriter writer;
        writer = new FileWriter(file, true);
        writer.append("\nНаименование трека " + this.name + "; проданные копии: " + this.copies + "; ");
        if (this.mp3Available)
            writer.append("есть в mp3 формате.\n");
        else writer.append("нет в mp3 формате.\n");
        writer.flush();
        writer.close();
    }

    //статический метод вывода информации из файла
    public static void OutputOfFile() throws IOException {
        File file = new File("document.doc");
        //поток для вывода информации
        FileReader reader;
        char buffer[];
        int numb;
        buffer = new char[1];
        reader = new FileReader(file);
        do {
            numb = reader.read(buffer);
            System.out.print(buffer[0]);
        } while (numb == 1);
        reader.close();
    }

    public static void main(String[] args) throws IOException {
        Music[] drugstores;
        drugstores = new Music[3];
        for (int i = 0; i < 3; i++)
            drugstores[i] = new Music();
        for (int i = 0; i < 3; i++)
            drugstores[i].InputInFile();
        OutputOfFile();
    }
}
