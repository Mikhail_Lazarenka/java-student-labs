package labs.lab5.example;

import java.io.*;

public class Drugstore {
    String name;
    double profit;
    boolean license;

    Drugstore() throws IOException {
        String bolv;
        //открываем символьный поток ввода
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in, "Cp1251"));
        System.out.print("\nВведите фамилию: ");
        this.name = input.readLine();
        System.out.print("\nВведите прибыль: ");
        this.profit = Double.parseDouble(input.readLine());
        while (true) {
            System.out.print("\nНаличие лицензии(Да/Нет): ");
            bolv = input.readLine();
            if ("Да".equals(bolv)) {
                this.license = true;
                break;
            }
            if ("Нет".equals(bolv)) {
                this.license = false;
                break;
            }
            System.out.print("\nОшибка! Повторите ввод");
        }
    }

    //записываем информацию в файл document.doc
    public void InputInFile() throws IOException {
        File file = new File("document.doc");
        file.deleteOnExit(); //файл удалится после завершения работы виртуальной машины Java
        //поток для записи в файл
        FileWriter writer;
        writer = new FileWriter(file, true);
        writer.append("\nВладелец аптеки " + this.name + "; прибыль: " + this.profit + "; ");
        if (this.license)
            writer.append("есть лицензия.\n");
        else writer.append("нет лицензии.\n");
        writer.flush();
        writer.close();
    }

    //статический метод вывода информации из файла
    public static void OutputOfFile() throws IOException {
        File file = new File("document.doc");
        //поток для вывода информации
        FileReader reader;
        char buffer[];
        int numb;
        buffer = new char[1];
        reader = new FileReader(file);
        do {
            numb = reader.read(buffer);
            System.out.print(buffer[0]);
        } while (numb == 1);
        reader.close();
    }

    public static void main(String[] args) throws IOException {
        Drugstore[] drugstores;
        drugstores = new Drugstore[3];
        for (int i = 0; i < 3; i++)
            drugstores[i] = new Drugstore();
        for (int i = 0; i < 3; i++)
            drugstores[i].InputInFile();
        OutputOfFile();
    }
}
