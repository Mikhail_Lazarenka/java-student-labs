package labs.lab4.task;

public abstract class Motivational implements Object, Book {
    protected String type;

    public Motivational( String type) {
        this.type = type;
    }
    public abstract String GetType();

}
