package labs.lab4.task;

public class TimeManagement extends Motivational{
    private String name;

    public TimeManagement(String type) {
        super(type);
    }

    @Override
    public String GetType() {
        return this.type;
    }

    @Override
    public String name() {
        return "Fiction book name: "  + this.name;
    }

    @Override
    public void print() {
        System.out.println("TimeManagement book info | " + this.name() + " type: "+ this.GetType());
    }
}
