package labs.lab4.task;



public class FictionBook implements Object, Book {
    private String name;
    private String genre;

    public FictionBook(String name, String genre) {
        this.name = name;
        this.genre = genre;
    }

    public FictionBook() {
    }

    public String getGenre() {
        return genre;
    }

    @Override // аннтотация указывающая что данный метод переопределяют
    public String name() {
        return "Fiction book name: "  + this.name;
    }

    @Override
    public void print() {
        System.out.println("Fiction book info | " + this.name() + " genre: "+ this.genre);
    }
}
