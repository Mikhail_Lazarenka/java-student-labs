package labs.lab4.task;

public class Sport extends Motivational {
    private String name;
    public Sport(String type) {
        super(type);
    }

    @Override
    public String GetType() {
        return this.type;
    }

    @Override
    public String name() {
        return "Sport book name: "  + this.name;
    }

    @Override
    public void print() {
        System.out.println("Sport book info | " + this.name() + " type: "+ this.GetType());
    }
}
