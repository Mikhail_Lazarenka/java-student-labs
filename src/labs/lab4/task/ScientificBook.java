package labs.lab4.task;


public class ScientificBook implements Object, Book {
    private String name;
    private String subject;
    private String complexity;

    public ScientificBook() {
    }

    public ScientificBook(String name, String subject, String complexity) {
        this.name = name;
        this.subject = subject;
        this.complexity = complexity;
    }

    public String getSubject() {
        return subject;
    }

    public String getComplexity() {
        return complexity;
    }

    @Override // аннтотация указывающая что данный метод переопределяют
    public String name() {
        return "Scientific book name: "  + this.name;
    }

    @Override
    public void print() {
        System.out.println("Scientific book info | " + this.name() + " subject: "+ this.subject + " complexity: "+ this.complexity);
    }
}
