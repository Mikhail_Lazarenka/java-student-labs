package labs.lab4.example;

public class UniversalTriangle extends Triangle {
    UniversalTriangle(double x, double y, double z, String type) {
        super(x, y, z, type);//вызов конструктора супер-класса
    }//конструктор класса

    public float FindSquare() {
        return (float) Math.sqrt(((x+y+z)/2)*(((x+y+z)/2)-x)*(((x+y+z)/2)-y)*(((x+y+z)/2)-z));
    }// реализация абстрактного метода

    public void printInfo() {
        System.out.println("Сторона1 - "+GetX()+" Сторона2 -"+GetY()+" Сторона3 - "+GetZ()+" Тип - " +GetType()+" Площадь - "+FindSquare());
    }//реализация абстрактного метода
    public String GetType(){
        return type;
    }// реализация абстрактного гет-метода

}
