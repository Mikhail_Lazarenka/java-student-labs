package labs.lab4.example;

import java.util.Scanner;

public class InterfaceDemo {
    public static void main(String[] args) {

        Circle circ1 = new Circle(2);
        circ1.printInfo();
        Rectangle rect1 = new Rectangle(3, 4);
        rect1.printInfo(); //создание объектов круг и прямоугольник, вызов их методов
        System.out.println("Для создания треугольника:");
        Scanner scan = new Scanner(System.in);
        Double i = 0d;
        System.out.println("Введитесторону1/катет1:");
        if (scan.hasNextDouble()) {
            i = scan.nextDouble();
        }

        Double j = 0d;
        System.out.println("Введите сторону2/катет2:");

        if (scan.hasNextDouble()) {
            j = scan.nextDouble();
        }

        Double k = 0d;
        System.out.println("Введите сторону3/гипотенузу:");


        if (scan.hasNextDouble()) {
            k = scan.nextDouble();
        }
        if (i >= j + k || j >= i + k || k >= i + j) {
            System.out.println("Такого треугольника не существует");
        }// проверка треугольника на существование, вывод сообщения
        else {
            if (i == j && i == k) {
                System.out.println("Вы создали равносторонний треугольник");
                EquilateralTriangle trian1 = new EquilateralTriangle(i, j, k, "равносторонний");
                trian1.printInfo();
            } else {
                if (k * k == i * i + j * j) {
                    System.out.println("Вы создали прямоугольный треугольник");
                    RightAngledTriangle trian1 = new RightAngledTriangle(i, j, k, "прямоугольный");
                    trian1.printInfo();
                } else {
                    System.out.println("Вы создали треугольник");
                    if (i == j || i == k || j == k) {
                        UniversalTriangle trian1 = new UniversalTriangle(i, j, k, "равнобедренный");
                        trian1.printInfo();
                    } else {
                        UniversalTriangle trian1 = new UniversalTriangle(i, j, k, "разносторонний");
                        trian1.printInfo();
                    }
                }
            }//создание треугольника правильного вида, согласно введённым пользователем сторонам, вызов его методов
        }

    }
}
