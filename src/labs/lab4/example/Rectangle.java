package labs.lab4.example;

public class Rectangle implements Object, GeomFig { // класс Rectangle наследует класс GeomFig
    private double a, b;
    public Rectangle(int a, int b){
        this.a=a;
        this.b=b;
    }// конструктор класса
    public double GetA() {
        return a;
    }// гет-метод для возврата значения одного из параметров прямоугольника
    public double GetB() {
        return b;
    }// гет-метод для возврата значения одного из параметров прямоугольника

    public float FindSquare() {
        return (float) (a*b);
    }//реализация абстрактного  метода нахождения площади  фигуры для прямоугольника


    public void printInfo() {
        System.out.println("Сторона1 - "+GetA()+" Сторона2 - "+GetB()+" Площадь - "+FindSquare());
    }



}

