package labs.lab4.example;

public abstract class Triangle implements Object, GeomFig { //класс Triangle наследует класс GeomFig
    public double x, y, z;
    String type;

    public Triangle(double x, double y, double z, String type) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.type = type;
    }// конструктор супер-класса 

    public double GetX() {
        return x;
    }//гет-метод 

    public double GetY() {
        return y;
    }//гет-метод

    public double GetZ() {
        return z;
    }//гет-метод

    public abstract String GetType();
//абстрактный гет-метод
}
