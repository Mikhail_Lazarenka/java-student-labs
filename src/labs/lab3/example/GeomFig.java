package labs.lab3.example;

public class GeomFig { //создание суперкласса
    protected int x,y;
    float square;
    public GeomFig(int x1,int y1){
        x=x1;
        y=y1;
    }
    public GeomFig(){
        x=5;
        y=5;
    }
    public int GetDatax(){
        return x;
    }
    public int GetDatay(){
        return y;
    }
    public float FindSquare () { // метод расчета площади, который будет //переопределяться в подклассах
        square=x*y;
        return square;
    }
}
