package labs.lab3.example;

public class Circle extends GeomFig {
    int rad;
    public Circle(int x, int y, int r){
        super(x,y);// вызов конструктора с параметрами из суперкласса
        rad=r;
    }
    public Circle(int x, int y){
        super(x,y);// вызов конструктора с параметрами из суперкласса
        rad=3;
    }
    public Circle(){
        super();//вызов конструктора без параметров из суперкласса
        rad=2;
    }

    public int GetDataRad(){
        return rad;
    }
    public float FindSquare () { //переопределение метода расчета площади
        square=(float) (3.14*rad*rad);
        return square;
    }

}
