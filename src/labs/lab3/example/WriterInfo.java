package labs.lab3.example;
/*
Создайте класс «Геометрическая фигура», содержащий следующую информацию: координаты центра на плоскости. Предусмотреть get методы и метод класса «расчет площади». Этот метод должны переопределять производные классы.
Создайте класс «Треугольник» производный от «Геометрическая фигура» содержит дополнительную информацию: сторона1, сторона2, сторона 3, тип.
Создайте класс «Прямоугольник» производный от «Геометрическая фигура» содержит дополнительную информацию: ширина, высота.
Создайте класс «Окружность» производный от «Геометрическая фигура» содержит дополнительную информацию: радиус.

 */
public class WriterInfo { // класс для вывода информации о классах на экран
    public static void main(String[] args) {
        Rectangle rect1= new Rectangle(1,2,3,4);
        System.out.println("Rectangle 1 - Center:("+rect1.GetDatax()+", "+rect1.GetDatay()+") Weidth:"+rect1.GetDatawidth()+" Height:"+rect1.GetDataheight()+" Square:"+rect1.FindSquare()+"; ");
        Rectangle rect2=new Rectangle(7,6,5);
        System.out.println("Rectangle 2 - Center:("+rect2.GetDatax()+", "+rect2.GetDatay()+") Weidth:"+rect2.GetDatawidth()+" Height:"+rect2.GetDataheight()+" Square:"+rect2.FindSquare()+"; ");
        Rectangle rect3=new Rectangle();
        System.out.println("Rectangle 3 - Center:("+rect3.GetDatax()+", "+rect3.GetDatay()+") Weidth:"+rect3.GetDatawidth()+" Height:"+rect3.GetDataheight()+" Square:"+rect3.FindSquare()+"; ");
        Circle circ1=new Circle(2,2,4);
        System.out.println("Circle 1 - Center:("+circ1.GetDatax()+", "+circ1.GetDatay()+") Radius:"+circ1.GetDataRad()+" Square:"+circ1.FindSquare()+"; ");
        Circle circ2=new Circle(4,1);
        System.out.println("Circle 2 - Center:("+circ2.GetDatax()+", "+circ2.GetDatay()+") Radius:"+circ2.GetDataRad()+" Square:"+circ2.FindSquare()+"; ");
        Circle circ3=new Circle();
        System.out.println("Circle 3 - Center:("+circ3.GetDatax()+", "+circ3.GetDatay()+") Radius:"+circ3.GetDataRad()+" Square:"+circ3.FindSquare()+"; ");
        Triangle trin1=new Triangle (4,5,6,7,8,"scalene");
        System.out.println("Triangle 1 - Center:("+trin1.GetDatax()+", "+trin1.GetDatay()+") Side 1:"+trin1.GetDataa()+" Side 2:"+trin1.GetDatab()+" Side 3:"+trin1.GetDatac()+" Type:"+trin1.Gettype()+" Square:"+trin1.FindSquare()+"; ");
        Triangle trin2=new Triangle(15,6);
        System.out.println("Triangle 2 - Center:("+trin2.GetDatax()+", "+trin2.GetDatay()+") Side 1:"+trin2.GetDataa()+" Side 2:"+trin2.GetDatab()+" Side 3:"+trin2.GetDatac()+" Type:"+trin2.Gettype()+" Square:"+trin2.FindSquare()+"; ");
        Triangle trin3=new Triangle();
        System.out.println("Triangle 3 - Center:("+trin3.GetDatax()+", "+trin3.GetDatay()+") Side 1:"+trin3.GetDataa()+" Side 2:"+trin3.GetDatab()+" Side 3:"+trin3.GetDatac()+" Type:"+trin3.Gettype()+" Square:"+trin3.FindSquare()+"; ");
    }
}

