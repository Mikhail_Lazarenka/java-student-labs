package labs.lab3.example;

public class Rectangle extends GeomFig{ // класс Rectangle наследует класс GeomFig
    private int width, height;
    public Rectangle(int x, int y, int w, int h) {
        super(x,y); //вызов конструктора с параметрами из суперкласса
        width=w;
        height=h;
    }
    public Rectangle (int x, int y, int w){
        super(x,y);// вызов конструктора с параметрами из суперкласса
        width=w;
        height=8;
    }
    public Rectangle() {
        super();//вызов конструктора без параметров из суперкласса
        width=7;
        height=6;
    }
    public int GetDatawidth(){
        return width;
    }
    public int GetDataheight(){
        return height;
    }
    public float FindSquare (){
        square=width*height;
        return square;
    }


}

