package labs.lab3.example;

public class Triangle extends GeomFig { //класс Triangle наследует класс GeomFig
    private int a,b,c;
    String type;
    public Triangle (int x, int y,int a, int b, int c, String type) {
        super(x,y); //вызов конструктора с параметрами из суперкласса
        this.a=a;
        this.b=b;
        this.c=c;
        this.type=type;
    }
    public Triangle (int x, int y) {
        super(x,y);// вызов конструктора с параметрами из суперкласса
        a=3;
        b=5;
        c=5;
        type="isosceles";
    }
    public Triangle () {
        super();//вызов конструктора без параметров из суперкласса
        a=7;
        b=7;
        c=7;
        type="equilateral";
    }
    public int GetDataa(){
        return a;
    }
    public int GetDatab(){
        return b;
    }
    public int GetDatac(){
        return c;
    }
    public String Gettype(){
        return type;
    }
    public float FindSquare (){ //переопределение метода расчета площади
        square=(float) Math.sqrt(((a+b+c)/2)*(((a+b+c)/2)-a)*(((a+b+c)/2)-b)*(((a+b+c)/2)-c));
        return square;
    }
}
