package labs.lab3.task.v2;

public class FictionBook extends Book {
    private String genre;

    public FictionBook(String name, String publishingHouse, String author, int pages, String genre) {
        super(name, publishingHouse, author, pages);
        this.genre = genre;
    }

    public FictionBook(String name, String publishingHouse, String author, int pages){
        super(name, publishingHouse, author, pages);
        this.genre = "Adventure";
    }

    public FictionBook() {
        super();
        this.genre = "Comedy";
    }

    public String getGenre() {
        return genre;
    }

    @Override // аннтотация указывающая что данный метод переопределяют
    public String name() {
        return "Fiction book name: "  + name;
    }

}
