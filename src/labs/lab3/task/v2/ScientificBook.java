package labs.lab3.task.v2;

public class ScientificBook extends Book {
    private String subject;
    private String complexity;

    public ScientificBook(String name, String publishingHouse, String author, int pages, String subject, String complexity) {
        super(name, publishingHouse, author, pages);
        this.subject = subject;
        this.complexity = complexity;
    }

    public ScientificBook(String name, String publishingHouse, String author, int pages) {
        super(name, publishingHouse, author, pages);
        this.subject = "Physics";
        this.complexity = "Easy";
    }

    public ScientificBook() {
        super();
        this.subject = "Biology";
        this.complexity = "High";
    }

    public String getSubject() {
        return subject;
    }

    public String getComplexity() {
        return complexity;
    }

    @Override
    public String name() {
        return "Scientific book name: " + name;
    }
}
