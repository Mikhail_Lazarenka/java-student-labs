package labs.lab3.task.v2;

public class Book {
    protected String name;
    protected String publishingHouse; //издательство
    protected String author;
    protected int pages;


    public Book() {
        this.name = "Super name";
        this.publishingHouse = "Super publishing house";
        this.author = "Super author";
        this.pages = 100;
    }

    public Book(String name, String publishingHouse, String author, int pages) {
        this.name = name;
        this.publishingHouse = publishingHouse;
        this.author = author;
        this.pages = pages;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public String getAuthor() {
        return author;
    }

    public int getPages() {
        return pages;
    }

    public String name() {
        return "The book name: " + this.name;
    }
}
