package labs.lab3.task.v2;

/*
Создайте класс «Книга», который содержит следующую информацию:
    издательство, автор и количество страниц. Предусмотреть get методы.
    Метод класса «Название», который потом будут переопределять производные классы.
Создайте класс «Художественная книга» производный от «Книга» содержит дополнительную информацию: жанр.
Создайте класс «Научная книга» производный от «Книга» содержит дополнительную информацию: тематика, сложность.

 */
public class Main {
    public static void main(String[] args) {
        Book book1 = new Book("BookName1","Publishing house 1","Michelangelo",123);
        System.out.println("Book 1 - "+ book1.name() + " author: " + book1.getAuthor()+ " : " + book1.getPublishingHouse()+ " pages: " + book1.getPages() );

        Book book2 = new Book("BookName2", "Publishing house 2","Paul",122);
        System.out.println("Book 2 - "+ book2.name() + " author: " + book2.getAuthor()+ " : " + book2.getPublishingHouse()+ " pages: " + book2.getPages() );

        Book book3=new Book();
        System.out.println("Book 3 - "+ book3.name());
//
        FictionBook fictionBook1 = new FictionBook("FictionBookName1","Publishing house 1","Michelangelo",123);
        System.out.println("FictionBook 1 - "+ fictionBook1.name() + " author: " + fictionBook1.getAuthor()+ " : " + fictionBook1.getPublishingHouse()+ " pages: " + fictionBook1.getPages() );

        FictionBook fictionBook2 = new FictionBook("FictionBookName2","Publishing house 2","Paul",122);
        System.out.println("FictionBook 2 - "+ fictionBook2.name() + " author: " + fictionBook2.getAuthor()+ " : " + fictionBook2.getPublishingHouse()+ " pages: " + fictionBook2.getPages() );

        FictionBook fictionBook3 = new FictionBook();
        System.out.println("FictionBook 3 - "+ fictionBook3.name() + " author: " + fictionBook3.getAuthor()+ " : " + fictionBook3.getPublishingHouse()+ " pages: " + fictionBook3.getPages() );
//
        ScientificBook scientificBook1 = new ScientificBook("ScientificBookName1","Publishing house 1","Michelangelo",123);
        System.out.println("ScientificBook 1 - "+ scientificBook1.name() + " author: " + scientificBook1.getAuthor()+ " : " + scientificBook1.getPublishingHouse()+ " pages: " + scientificBook1.getPages() );

        ScientificBook scientificBook2 = new ScientificBook("ScientificBookName2","Publishing house 2","Paul",122);
        System.out.println("ScientificBook 2 - "+ scientificBook2.name() + " author: " + scientificBook2.getAuthor()+ " : " + scientificBook2.getPublishingHouse()+ " pages: " + scientificBook2.getPages() );

        ScientificBook scientificBook3 =new ScientificBook();
        System.out.println("ScientificBook 3 - "+ scientificBook3.name() + " author: " + scientificBook3.getAuthor()+ " : " + scientificBook3.getPublishingHouse()+ " pages: " + scientificBook3.getPages() );
    }
}
