package labs.lab3.task;

/*
Создайте класс «Книга», который содержит следующую информацию:
    издательство, автор и количество страниц. Предусмотреть get методы.
    Метод класса «Название», который потом будут переопределять производные классы.
Создайте класс «Художественная книга» производный от «Книга» содержит дополнительную информацию: жанр.
Создайте класс «Научная книга» производный от «Книга» содержит дополнительную информацию: тематика, сложность.

 */
public class Main {
    public static void main(String[] args) {
        Book book1 = new Book("Publishing house 1","Michelangelo",123);
        System.out.println("Book 1 - "+ book1.name());
        Book book2 = new Book("Publishing house 2","Paul",122);
        System.out.println("Book 2 - "+ book2.name());
        Book book3=new Book();
        System.out.println("Book 3 - "+ book3.name());

        FictionBook fictionBook1 = new FictionBook("Publishing house 1","Michelangelo",123);
        System.out.println("Fiction book 1 - "+ fictionBook1.name());
        FictionBook fictionBook2 = new FictionBook("Publishing house 2","Paul",122);
        System.out.println("Fiction book 2 - "+ fictionBook2.name());
        FictionBook fictionBook3=new FictionBook();
        System.out.println("Fiction book 3 - "+ fictionBook3.name());

        ScientificBook scientificBook1 = new ScientificBook("Publishing house 1","Michelangelo",123);
        System.out.println("Scientific book 1 - "+ scientificBook1.name());
        ScientificBook scientificBook2 = new ScientificBook("Publishing house 2","Paul",122);
        System.out.println("Scientific book 2 - "+ scientificBook2.name());
        ScientificBook scientificBook3 =new ScientificBook();
        System.out.println("Scientific book 3 - "+ scientificBook3.name());
    }
}
