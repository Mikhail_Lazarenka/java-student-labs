package labs.lab3.task;

public class FictionBook extends Book{
    private String genre;

    public FictionBook(String publishingHouse, String author, int pages, String genre) {
        super(publishingHouse, author, pages);
        this.genre = genre;
    }

    public FictionBook(String publishingHouse, String author, int pages){
        super(publishingHouse, author, pages);
        this.genre = "Adventure";
    }

    public FictionBook() {
        super();
        this.genre = "Comedy";
    }

    public String getGenre() {
        return genre;
    }

    @Override // аннтотация указывающая что данный метод переопределяют
    public String name() {
        return "Fiction book name | " +"publishing house: "+ publishingHouse + " author: " + author + " genre: "+genre +" pages: " + pages;
    }

}
