package labs.lab3.task;

public class Book {
    protected String publishingHouse; //издательство
    protected String author;
    protected int pages;


    public Book() {
        this.publishingHouse = "Super publishing house";
        this.author = "Super author";
        this.pages = 100;
    }

    public Book(String publishingHouse, String author, int pages) {
        this.publishingHouse = publishingHouse;
        this.author = author;
        this.pages = pages;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public String getAuthor() {
        return author;
    }

    public int getPages() {
        return pages;
    }

    public String name() {
        return "Book name | " +"publishing house: "+ publishingHouse + " author: " + author + " pages: " + pages;
    }
}
