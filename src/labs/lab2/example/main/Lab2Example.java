package labs.lab2.example.main;

import labs.lab2.example.Drugstore;
/*
Создать массив объектов класса согласно заданию.
Инициализацию значений полей осуществить с помощью различных конструкторов.
 В классе предусмотреть геттеры, метод, осуществляющий вывод на экран монитора соответствующую информацию об объекте и перегруженные конструкторы.
 Предусмотреть статические методы, подсчёта общей прибыли и количества аптек с лицензией . Главный класс с методом main должен находиться в другом пакете.
 Состав класса Drugstore (аптека): фамилия вла¬дельца, наличие лицензии, месячная прибыль.
 */
public class Lab2Example {
    public static void main(String[] args) {

        Drugstore[]d1 = new Drugstore[4]; //создаём массив из 10 объектов класса Drugstore
        d1[0] = new Drugstore("Kolenko", true, 100);  //создаём первый объект массива с помощью конструктора с полным набором параметров
        d1[1]=new Drugstore("Petrov",false); // аналогично заполняем другой, но теперь пользуемся конструктором без одного параметра
        d1[2]=new Drugstore();  // заполняем третий с помощью конструктора по умолчанию, все поля будут проинициализированы нулями
        d1[3]=new Drugstore(d1[0]); // вызов конструктора копий, здесь мы копируем информацию, из первого объекта
        for(int i=0;i<4;i++){
            d1[i].print();        }// выводим полученную информацию.
        System.out.println("sum of profits "+ Drugstore.Profit(d1));  //выводим суммы зарплаты через вызов статистического метода

        System.out.println("owners with license "+ Drugstore.hasLicense(d1));
//выводим количество владельцев с лицензиями через вызов статистического метода
    }
}
