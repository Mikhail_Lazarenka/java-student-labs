package labs.lab2.task.main;

import labs.lab2.task.Music;

public class Lab2 {
    public static void main(String[] args) {

        Music[] tracks = new Music[4]; //создаём массив из 10 объектов класса Music
        tracks[0] = new Music("(feat. Ollie Wride) FM-84 - Running in the Night " , true, 100);  //создаём первый объект массива с помощью конструктора с полным набором параметров
        tracks[1] = new Music("Макс Гирко x Anacondaz - Иди за второй", false); // аналогично заполняем другой, но теперь пользуемся конструктором без одного параметра
        tracks[2] = new Music();  // заполняем третий с помощью конструктора по умолчанию, все поля будут проинициализированы нулями
        tracks[3] = new Music(tracks[0]); // вызов конструктора копий, здесь мы копируем информацию, из первого объекта
        for (int i = 0; i < 4; i++) {
            tracks[i].print();
        }// выводим полученную информацию.
        System.out.println("Total number of copies: " + Music.totalCopies(tracks));

        System.out.println("The number of mp3 available tracks: " + Music.numberOfMp3Available(tracks));

    }
}
