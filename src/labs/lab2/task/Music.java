package labs.lab2.task;

public class Music {
    private String name;
    private boolean mp3Available;
    private int copies;

    public Music() {
    }

    public Music(String name, boolean mp3Available, int copies) {
        this.name = name;
        this.mp3Available = mp3Available;
        this.copies = copies;
    }
    public Music (Music o){
        this.name = o.name;
        this.mp3Available = o.mp3Available;
        this.copies = o.copies;
    }

    public Music(String name, boolean mp3Available) {
        this.name = name;
        this.mp3Available = mp3Available;
    }

    public String getName() {
        return name;
    }

    public boolean isMp3Available() {
        return mp3Available;
    }

    public int getCopies() {
        return copies;
    }

    public static int totalCopies( Music[] tracks){
        int total = 0;
        for (int i = 0; i < 4; i++)
            total += tracks[i].copies;
        return total;
    }

    public static int numberOfMp3Available (Music[] tracks){
        int number = 0;
        for (int i = 0; i < 4; i++)
            if (tracks[i].mp3Available == true)
                number++;
        return number;
    }

    public void print() {
        System.out.println("-- Track name: " + name);
        if (mp3Available) {
            System.out.println("Mp3 available.");
        } else {
            System.out.println("The track isn't mp3 available.");
        }
        System.out.println("Copies sold: " + copies);
    }
}
